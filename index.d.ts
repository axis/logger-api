declare module "@ayanaware/logger-api" {
	export enum LogLevel {
		OFF = "OFF",
		ERROR = "ERROR",
		WARN = "WARN",
		INFO = "INFO",
		DEBUG = "DEBUG",
		TRACE = "TRACE"
	}
	/**
	 * Logger main class. Use Logger.get() to create a new Logger.
	 */
	export class Logger {
		private constructor();
		/**
		 * Logs a message with [[LogLevel]] *ERROR*
		 *
		 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
		 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
		 * @param extra Optional. An object containing additional data that can be used later on
		 *
		 * @see Logger#log
		 */
		error(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any; }): void;
		/**
		 * Logs a message with [[LogLevel]] *WARN*
		 *
		 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
		 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
		 * @param extra Optional. An object containing additional data that can be used later on
		 *
		 * @see Logger#log
		 */
		warn(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any; }): void;
		/**
		 * Logs a message with [[LogLevel]] *INFO*
		 *
		 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
		 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
		 * @param extra Optional. An object containing additional data that can be used later on
		 *
		 * @see Logger#log
		 */
		info(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any; }): void;
		/**
		 * Logs a message with [[LogLevel]] *DEBUG*
		 *
		 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
		 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
		 * @param extra Optional. An object containing additional data that can be used later on
		 *
		 * @see Logger#log
		 */
		debug(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any; }): void;
		/**
		 * Logs a message with [[LogLevel]] *TRACE*
		 *
		 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
		 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
		 * @param extra Optional. An object containing additional data that can be used later on
		 *
		 * @see Logger#log
		 */
		trace(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any; }): void;
		/**
		 * Logs a message.
		 *
		 * @param level The log level
		 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
		 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
		 * @param extra Optional. An object containing additional data that can be used later on
		 */
		log(level: LogLevel, log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any; }): void;
		/**
		 * Creates a new logger instance for the ctrueurrent context.
		 *
		 * @param forClass A string or a Class for the loggers name
		 * @param extra Optional. An object containing additional data that will be appended on every log call
		 *
		 * @returns A new logger instance
		 */
		public static get(forClass?: string | Function, extra?: { [key: string]: any }): Logger;
	}

	export default Logger;
}
