@ayanaware/logger-api [![NPM](https://img.shields.io/npm/v/@ayana/logger-api.svg)](https://www.npmjs.com/package/@ayanaware/logger-api) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf) [![Install size](https://packagephobia.now.sh/badge?p=@ayana/logger-api)](https://packagephobia.now.sh/result?p=@ayanaware/logger-api) [![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo)
===

Logger API for usage in libraries

What this is
---

This is a really small module defining the API of [@ayanaware/logger](https://npmjs.com/package/@ayanaware/logger) for usage in libraries.

Why this exists
---

Some people who make a library might not want to force people to use `@ayanaware/logger` but still use it in their applications for logging. With `@ayanaware/logger-api` it is up to the user if he want's to use `@ayanaware/logger` or not. If the user doesn't install the `@ayanaware/logger` module themselves, libraries using `@ayanaware/logger-api` won't show any log output. However if the user does install it, the logging will work.

Technically the library user could just disable `@ayanaware/logger` but that is configuration effort and also leaves the module installed doing nothing and consuming disk space.

Installation
---

With NPM

```
npm i @ayanaware/logger-api
```

With Yarn

```
yarn add @ayanaware/logger-api
```

Usage
---

Look at the [README of the main module](https://npmjs.com/package/@ayanaware/logger) for usage. Note that some features aren't available for libraries as it is an anti-pattern to access them.

Links
---

[GitLab repository](https://gitlab.com/ayanaware/logger-api)

[NPM package](https://npmjs.com/package/@ayanaware/logger-api)

License
---

Refer to the [LICENSE](LICENSE) file.
